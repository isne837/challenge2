
#include<iostream>
#include<string>
using namespace std;
//width 7 height 6
int board[7][6];
int turn=1;

void gameOn();
void resetBoard();
void render();
void playerPutCoin(int,int);
int winCheck();
int rowWin();
int lineWin();
int diagonalWin();
int diagonalLeft();
int diagonalRight();
int draw();

int main() {
	char game;
	do{
		gameOn();	
		cout<<"PLAY ANOTHER ONE? (n) for End";
		cin>>game;
	}while(game != 'n');
}

void gameOn(){
	resetBoard();
	//render();
	int index,turnOn,winner;
	int player[2]={1,2};
	do{
		if(turn % 2 == 1) turnOn = player[0];
		else turnOn = player[1];
		
		
		cout<<"TURN :"<<turn<<"  Player :  "<<turnOn<<endl ;
		do{
			cin>>index;
			if(!(index >= 1 && index <=7)) cout<<"INDEX OUT OF RANGE"<<endl;
		}while(!(index >= 1 && index <=7));
		index--;
		playerPutCoin(index,turnOn);
		render();
		winner = winCheck();
		turn++;
	}while(winner == 0);
	if(winner != -1) cout <<"PLYERRRR "<< winner<<"  WINNNERRRRRRRRRR"<<endl<<endl<<endl;
}

void resetBoard(){
	for(int i=0;i<7;i++) for(int j=0;j<6;j++) board[i][j] = 0;
	turn = 1;
}

void render(){
	for(int i=0;i<7;i++) cout<<"|"<<i+1;
	cout<<"|"<<endl;
	for(int i=0;i<6;i++) {
		for(int j=0;j<7;j++) {
			cout<<"|";
			if(board[j][i] == 0) cout<<" ";
			else{
				if(board[j][i] == 1) cout<<"X";
				else cout<<"O";
			};
			if(j==6) cout<<"|";
		}
		cout<<endl;
	}
}

void playerPutCoin(int index,int player){
	//row full
	if(board[index][0] != 0){
		cout<<"THIS ROW IS FULL TRY TO OTHER INDEX"<<endl;
		int newIndex;
		cin>>newIndex;
		playerPutCoin(newIndex,player);	
	}else{
	   for(int i=5;i>=0;i--) {
	   		if(board[index][i]==0) {
	   			board[index][i] = player ;
	   			break;
		   }
	   }
	}
}

int winCheck(){
	if(draw() != 0){
		cout<<"DRAW"<<endl;
		return draw();
	}
	if(rowWin() != 0) {
		cout<<"ROW WIN"<<endl;
		return rowWin();
	}
	if(lineWin() != 0) {
		cout<<"LINE WIN"<<endl;
		return lineWin();
	}
	if(diagonalWin() != 0) {
		cout<<"DIAGONAL WIN"<<endl;
		return diagonalWin();
	}
	return 0;
}

int rowWin() {
	int rowCheck=0;
	for(int i=0;i<6;i++) {
		for(int j=0;j<7;j++) {
			if(board[j][i] == board[j+1][i] && j <= 5) rowCheck++;
			else rowCheck=0;
			if(rowCheck >= 3 && board[j][i] != 0) return board[j][i];
		}
	}
	return 0;
}

int lineWin() {
	int lineCheck=0;
	for(int j=0;j<7;j++) {
		for(int i=0;i<6;i++) {
			if(board[j][i] == board[j][i+1] && i <= 4) {
			lineCheck++;
			if(lineCheck >= 3 && board[j][i] != 0) return board[j][i];	
			}else lineCheck =0;
		}
		lineCheck=0;
	}
	return 0;
}

int diagonalWin() {
	if(diagonalLeft() != 0) return diagonalLeft();
	if(diagonalRight() != 0) return diagonalRight();
	return 0;
}

int diagonalRight(){
	for(int i=0;i<7;i++){
		for(int j=0;j<6;j++){
			if(board[i][j]==board[i+1][j+1] && board[i][j] != 0 && i+3 < 7 && j+3 <6){
				if(board[i+1][j+1] == board[i+2][j+2]) if(board[i+2][j+2] == board[i+3][j+3]) return board[i][j];
			}
		}
	}
	return 0;
}
int diagonalLeft(){
	for(int i=0;i<7;i++){
		for(int j=6;j>=0;j--){
			if(board[i][j]==board[i+1][j-1] && board[i][j] != 0 && i+3 < 7 && j-3 >= 0){
				if(board[i+1][j-1] == board[i+2][j-2]) if(board[i+2][j-2] == board[i+3][j-3]) return board[i][j];
			}
		}
	}
	return 0;
}
int draw() {
	int count = 0;
	for(int i = 0;i<7;i++) if(board[i][0] != 0) count++;
	if(count == 7) return -1;
	return 0;
}
